<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class SeederCreate extends Command
{
    protected $signature = 'seeder:create {table} {fields}';
    protected $description = 'Create model';

    public function handle()
    {
    	// Receber variáveis
        $table = $this->argument('table');
        $fields = $this->argument('fields');        

        // Definir arquivo de stub
        $stub = app_path('Console/Commands/stubs/seeder.stub');        
		$string = file_get_contents($stub);
		
		// Mudar tabela para inicial maiúscula
		$tableUc = Str::of($table)->ucfirst();
		
		$fields = explode(',', $fields);
		$flds = '';
		foreach($fields as $field){
			$fak = "\$faker->name,";
			$flds .="'$field' => $fak";		
		}
		
		// Substituir {{model}} por $tableUcSing no stub
		$string = str_replace('{{table}}', $table, $string);
		$string = str_replace('{{tableUc}}', $tableUc, $string);
		$string = str_replace('{{fields}}', $flds, $string);		
	
		$seeder = database_path('seeders/'.$tableUc.'Seeder.php');				
		file_put_contents($seeder, $string);
		$this->info('=> Seeder criado com sucesso');
		
		// php artisan seeder:create clients
    }
}
