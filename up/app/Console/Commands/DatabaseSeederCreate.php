<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;

class DatabaseSeederCreate extends Command
{
    protected $signature = 'dbseeder:create {table}';
    protected $description = 'Create DbSeeder';

    public function handle()
    {
   		$this->info('=> Não use em aplicativo existente, pois remove arquivos');
    	// Receber variáveis
        $table = $this->argument('table');               

        // Definir arquivo de stub
        $stub = app_path('Console/Commands/stubs/db.stub');        
		$string = file_get_contents($stub);
		
		// Mudar tabela para inicial maiúscula
		$tableUc = Str::of($table)->ucfirst();

		// Substituir {{model}} por $tableUcSing no stub
		$string = str_replace('{{table}}', $table, $string);
		$string = str_replace('{{tableUc}}', $tableUc, $string);		
	
		$dbseeder = database_path('seeders/DatabaseSeeder.php');
		
		if(FILE::exists($dbseeder)){
			FILE::delete($dbseeder);
		}
		file_put_contents($dbseeder, $string);
		$this->info('=> DatabaseSeeder criado com sucesso');
		
		// php artisan seeder:create clients
    }
}
