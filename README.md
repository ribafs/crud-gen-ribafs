# CRUD generator - Laravel 8, 9, 10, 11

## Testado em

Testado nas versões 8, 9, 10 e 11

Em Wiindows 11 com Xampp e WSL2 e no Linux Debian 12

## Executar
```bash
composer create-project laravel/laravel crud
cd crud
composer require ribafs/crud-gen-ribafs
php artisan vendor:publish --provider="Ribafs\Crudgen\CrudgenServiceProvider"
```
## Ajustar o banco no .env

Ajuste a miration criada, o seeder e o controller, caso tenha algum campo cujo tipo seja diferente de string
```bash
php artisan crud:create products string#name,decimal#price
npm install
npm run dev
```
Ajustar o banco no .env

Ajuste a miration criada, caso tenha algum campo cujo tipo seja diferente de string

```bash
php artisan migrate --seed

Agora é o momento em que apareceram erros no seeder, caso a tabela criada tenha campos diferentes de name e price. Edite o seeder e ajuste, então repita o comando acima.

php artisan serve

http://127.0.0.1:8000
```

## License

MIT
