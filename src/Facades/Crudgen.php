<?php
namespace Ribafs\Crudgen\Facades;

use Illuminate\Support\Facades\Facade;

class Crudgen extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'crud-gen';
    }
}
