<?php
namespace Ribafs\Crudgen;

use Illuminate\Support\ServiceProvider;

class CrudgenServiceProvider extends ServiceProvider
{    
    public function boot()
    {
        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        //$this->commands();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['crud-gen'];
    }
    
    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Here only folders with new files and only copy without overwrite
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/crud-gen.php' => config_path('crud-gen.php'),
        ], 'crud-gen.config');

        // Directories
        // Publishing app.
        $this->publishes([
            __DIR__.'/../up/app/' => base_path('/app'),
        ], 'crud-gen.models');
  
        // Registering package commands.
        //$this->commands([]);
    }
}
